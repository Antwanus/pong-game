import time
from turtle import Screen

from ball import Ball
from paddle import Paddle
from scoreboard import Scoreboard

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

screen = Screen()
screen.bgcolor("black")
screen.setup(SCREEN_WIDTH, SCREEN_HEIGHT)
screen.title("PONG")
screen.tracer(0)

p_right = Paddle(x_pos=350)
p_left = Paddle(x_pos=-350)

screen.listen()
screen.onkeypress(p_right.go_up, "Up")
screen.onkeypress(p_right.go_down, "Down")
screen.onkeypress(p_left.go_up, "z")
screen.onkeypress(p_left.go_down, "s")

ball = Ball()
scoreboard = Scoreboard()

is_game_on = True
while is_game_on:
    time.sleep(ball.move_speed)
    screen.update()
    ball.move()

    # handle ball hits top or bottom
    has_ball_hit_top = ball.ycor() > 280
    has_ball_hit_bottom = ball.ycor() < -280
    if has_ball_hit_top or has_ball_hit_bottom:
        ball.bounce()

    # handle ball hits paddle
    has_ball_hit_right_paddle = ball.distance(p_right) < 50 and ball.xcor() > 340
    has_ball_hit_left_paddle = ball.distance(p_left) < 50 and ball.xcor() < -340
    if has_ball_hit_right_paddle or has_ball_hit_left_paddle:
        ball.hit()

    # handle ball out of bounds (paddle misses)
    has_p_right_missed = ball.xcor() > 380
    has_p_left_missed = ball.xcor() < -380
    if has_p_right_missed:
        ball.reset_position()
        scoreboard.p_left_wins()

    if has_p_left_missed:
        ball.reset_position()
        scoreboard.p_right_wins()

screen.exitonclick()
