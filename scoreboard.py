from turtle import Turtle


class Scoreboard(Turtle):
    def __init__(self):
        super().__init__()
        self.penup()
        self.color("white")
        self.hideturtle()
        self.p_left_score = 0
        self.p_right_score = 0
        self.update_scoreboard()

    def update_scoreboard(self):
        self.clear()
        self.setpos(-100, 200)
        self.write(self.p_left_score, align="center", font=("Courier", 40, "bold"))
        self.setpos(100, 200)
        self.write(self.p_right_score, align="center", font=("Courier", 40, "bold"))

    def p_left_wins(self):
        self.p_left_score += 1
        self.update_scoreboard()

    def p_right_wins(self):
        self.p_right_score += 1
        self.update_scoreboard()
